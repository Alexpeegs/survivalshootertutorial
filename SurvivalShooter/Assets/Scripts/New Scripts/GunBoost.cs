﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBoost : MonoBehaviour {

    public float boosttime = 0.8f;
    public GameObject pickupEffecter;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Pickup(other);
        }
    }
    void Pickup(Collider player)
    {
        Instantiate(pickupEffecter, transform.position, transform.rotation);



        PlayerShooting shooting = player.GetComponentInChildren<PlayerShooting>();
        shooting.timeBetweenBullets *= boosttime;


        Destroy(gameObject);








    }
}
