﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke ("DestroyMe", 3f);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void DestroyMe ()
	{
		GameObject.Destroy (gameObject);
	}
}
